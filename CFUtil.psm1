function Download([String] $url, [String] $loc, [String] $filename) {
	# Download the file to a specific location
	
	$clnt = new-object System.Net.WebClient
	$file = Join-Path $loc $filename
	
	Write-Output "Downloading $url to $file"
	$clnt.DownloadFile($url,$file)
}

function UnZip ([String] $file, [String] $extractTo) {
	# Unzip the file to specified location
	
	Write-Output "Extracting $file to $extractTo"
	$shell_app=new-object -com shell.application 
	$zip_file = $shell_app.namespace($file) 
	$destination = $shell_app.namespace($extractTo)	
	$silentOverwrite = 0x14
	$destination.Copyhere($zip_file.items(), $silentOverwrite)
}

function CopyFolder([String] $src, [String] $dst) {
	# Copy the src folder to dst recursively
	
	Write-Output "Copying $src recursively to $dst"
	Copy-Item $src $dst -Recurse
}

function CreateFolder([String] $path) {
	If(!(test-path $path))
	{
		md $path
	}
}